start:

	@python3.9 -m http.server

deploy-assets:

	@scripts/deploy-assets.sh

download-videos:

	@rm -rf assets/videos/raw/
	@echo 'Downloading all three playlists in parallel...'
	@echo '************************************'
	@make download-videos-1 &
	@make download-videos-2 &
	@make download-videos-3 &


download-videos-1:

	@mkdir -p assets/videos/raw/
	cd assets/videos/raw/ && \
		youtube-dl -cit -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4' \
		https://www.youtube.com/playlist\?list\=PL791FyOwS4VjpgUzHCOGG-KLc8RSU_ro1
	
download-videos-2:

	@mkdir -p assets/videos/raw/
	cd assets/videos/raw/ && \
		youtube-dl -cit -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4' \
		https://www.youtube.com/playlist\?list\=PL791FyOwS4VgPR9LE7ygakA4BzuBiOVmF

download-videos-3:

	@mkdir -p assets/videos/raw/
	cd assets/videos/raw/ && \
		youtube-dl -cit -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4' \
		https://www.youtube.com/playlist\?list\=PL791FyOwS4Vh8xBplHoOmKJAYsaQ5Ix66

process-videos:

	@python scripts/process-videos.py

start-stream:

	FileGlobLivestream assets/sounds/ youtube -glob "*.mp3" -image assets/dw-full.png 
document.addEventListener('DOMContentLoaded', function() {

    //-----------//
    // CONSTANTS //
    //-----------//

    // modal elements
    const modal = document.getElementById('modal');
    const modalButton = modal.getElementsByClassName('modal-button')[0];

    // video elements 
    const assetPath = "https://gltd-3-dekalb-works-duologue.nyc3.digitaloceanspaces.com/assets/";
    const videoPath = assetPath + "videos/processed/";
    const videoJSON = assetPath + "data/videos.json";
    const videoA = document.getElementById("video-a");
    const videoB = document.getElementById("video-b");
    const videoC = document.getElementById("video-c");
    const videoD = document.getElementById("video-d");
    const videoE = document.getElementById("video-e");
    const videoF = document.getElementById("video-f");
    const videoG = document.getElementById("video-g");

    // audio elements 
    const audioPath = assetPath + "sounds/"
    const audio = document.getElementById("audio-player")


    //-----------//
    // UTILITIES //
    //-----------//

    const getRandom = function(items) {
        return items[Math.floor(Math.random() * items.length)];
    }

    const getVideoURL = function(vid) {
        return videoPath + vid.slug + '.' + vid.ext;
    }

    //-------------//
    // VIDEO LOGIC //
    //-------------//

    const setVideoPortrait = function(vidElem, videos) {
        const vid = getRandom(videos.filter(vid => vid.fields.ffp_is_portrait == true));
        var source = vidElem.getElementsByTagName('source')[0]
        var vidURL = getVideoURL(vid);
        // console.log('setting portrait vid: ' + vidURL);
        source.setAttribute('src', vidURL);
        source.setAttribute('type', vid.mimetype);
        vidElem.load();
        vidElem.play();
    }

    const setVideoLandscape = function(vidElem, videos) {
        const vid = getRandom(videos.filter(vid => vid.fields.ffp_is_portrait == false));
        var source = vidElem.getElementsByTagName('source')[0]
        var vidURL = getVideoURL(vid);
        // console.log('setting landscape vid: ' + vidURL);
        source.setAttribute('src', vidURL);
        source.setAttribute('type', vid.mimetype);
        vidElem.load();
        vidElem.play();
    }

    const setVideoA = function(videos) {
        setVideoPortrait(videoA, videos);
    }

    const setVideoB = function(videos) {
        setVideoPortrait(videoB, videos)
    }

    const setVideoC = function(videos) {
        setVideoPortrait(videoC, videos);
    }

    const setVideoD = function(videos) {
        setVideoLandscape(videoD, videos);
    }

    const setVideoE = function(videos) {
        setVideoLandscape(videoE, videos);
    }

    const setVideoF = function(videos) {
        setVideoLandscape(videoF, videos);
    }

    const setVideoG = function(videos) {
        setVideoLandscape(videoG, videos);
    }

    const setAllVideos = function(videos) {
        setVideoA(videos);
        setVideoB(videos);
        setVideoC(videos);
        setVideoD(videos);
        setVideoE(videos);
        setVideoF(videos);
        setVideoG(videos);
    }

    const loadVideos = function(callback) {
        fetch(videoJSON)
            .then(response => response.json())
            .then(videos => callback(videos))
            .catch(error => console.log('ERROR DURING FETCH JSON: ' + error));

    }

    // event listeners 
    videoA.addEventListener('ended', function() {
        loadVideos(setVideoA)
    });
    videoB.addEventListener('ended', function() {
        loadVideos(setVideoB);
    });
    videoC.addEventListener('ended', function() {
        loadVideos(setVideoC);
    });
    videoD.addEventListener('ended', function() {
        loadVideos(setVideoD);
    });
    videoE.addEventListener('ended', function() {
        loadVideos(setVideoE);
    });
    videoF.addEventListener('ended', function() {
        loadVideos(setVideoF);
    });
    videoG.addEventListener('ended', function() {
        loadVideos(setVideoG);
    });
    //-------------//
    // AUDIO LOGIC //
    //-------------//

    const getCurrentHourInEst = function() {
        var offset = new Date().getTimezoneOffset(); // getting offset to make time in gmt+0 zone (UTC) (for gmt+5 offset comes as -300 minutes)
        var date = new Date();
        date.setMinutes(date.getMinutes() + offset); // date now in UTC time
        var easternTimeOffset = -300;
        date.setMinutes(date.getMinutes() + easternTimeOffset);
        return date.getHours() + 1;
    }
    const getCurrentSecondsOffset = function() {
        var date = new Date();
        return date.getMinutes() * 60 + date.getSeconds();
    }
    const getCurrentSoundPath = function() {
        return audioPath + getCurrentHourInEst() + "_DW_24HRS.mp3#t=" + getCurrentSecondsOffset();
    };

    var loadAudio = function() {
        // Set the src of the audio tag to the current hour's track
        audio.src = getCurrentSoundPath();
        audio.load(); //call this to just preload the audio without playing
    }

    // restart the next audio file if it ends at the hour.
    audio.addEventListener('ended', function(event) {
        loadAudio();
        audio.play();
    })

    //-------------//
    // MODAL LOGIC //
    //-------------//

    const modalButtonOnClick = function(event) {
        // close the modal 
        modal.style.display = "none";

        // set the sound source

        // set the video sources 
        loadVideos(setAllVideos);

        // initially load the audio to save on 
        // processing time when listener
        // eventually clicks.
        loadAudio();
        audio.play();
    }

    modal.addEventListener("click", modalButtonOnClick);
    modalButton.addEventListener("click", modalButtonOnClick);


}, false);
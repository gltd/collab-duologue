
# [DeKalb Works - Duolouge (gltd.cat/2)](https://gltd.cat/2)

Code that powers the audio/visual release for Dekalb Work's 'Duoogue'
- [dekalb.works](https://dekalb.works)

The site orchestrates three years of archived sound and image collected into an online portal which transports the viewers into the artist's archive. Snippets of footage are presented as a moving sculpture set to an infinitely-repeating 24-hour stream of the Album, with interspersed outtakes from the Aritsts. Each hour the album starts anew, when it ends a new segment is presented. The album runs for approximately 45 minutes.


## How it was made.

Videos were sourced by the artists and compiled into three youtube playlists:

1. https://www.youtube.com/playlist?list=PL791FyOwS4VjpgUzHCOGG-KLc8RSU_ro1
2. https://www.youtube.com/playlist?list=PL791FyOwS4VgPR9LE7ygakA4BzuBiOVmF
3. https://www.youtube.com/playlist?list=PL791FyOwS4Vh8xBplHoOmKJAYsaQ5Ix66) 


The videos in these playlists are downloaded using `youtube-dl` (see the [`Makefile`](Makefile) for the specific commands). They're then processed using the `dada-video` package in [`dada-lake`](https://gitlab.org/gltd/dada-lake). Processing steps included reducing filesizes, chunking longer videos into 1-minute segments, and extracting metadata (width, height, orientation, etc). This results in a [JSON file of videos](assets/data/videos.json) which is loaded by the site.

The site combines the processed videos and 24 1-hour audio segments provided by the artists using [javascript](assets/js/main.js). We cycle through the videos by attaching random-video picking functions to `ended` events on each video element. This way a new video is loaded once the previous one ends. We limit videos of particular orientations (landscape vs. portrait) in order to retain the sculuptural effect of the grid.
 
In order to simulate an infinitely-repeating livestream, we make use of the HTML 5 audio element's built-in time-seeking by appending the current nnumber of seconds since the beginning of the current hour to the current hour's audio url. For instance, if it were 2:02 PM when the page loads, we'd set the audio url to `file_14.mp3#t=120`. This creates the effect of a livestream (every user will be listening to the same sounds at roughly the same time) without the need for a central source. 


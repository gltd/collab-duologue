#!/usr/bin/env python
import shlex

import dada_file
import dada_video
import dada_serde
from dada_utils import path
from dada_stor import DadaStor
from dada_log import DadaLogger


RAW_VIDEO_DIR = path.here(__file__, "../assets/videos/raw/")
PROC_VIDEO_DIR = path.here(__file__, "../assets/videos/processed/")
IMAGE_DIR = path.here(__file__, "../assets/img/processed/")

stor = DadaStor(bucket_name="gltd-assets")
log = DadaLogger(name="duologue-process-video")


def process_vid(fp):
    """
    Process a video from raw youtube-dl downloads
    resizing + converting everything into mp4.
    """
    fp_df = dada_file.load(fp)
    log.info(f"Running ffprobe on {fp}")
    fields = dada_video.ffprobe_get_fields(fp)
    fp_df.ensure_dada(fp)
    fp_df.dada["fields"] = fields

    # set portrait mode
    if (
        fp_df.dada["fields"]["ffp_video_height"]
        > fp_df.dada["fields"]["ffp_video_width"]
    ):
        fp_df.dada["fields"]["ffp_is_portrait"] = True
    else:
        fp_df.dada["fields"]["ffp_is_portrait"] = False

    return fp_df.dada


def main():
    """
    List all the videos, extract metadata, + take shot of first frame.
    """
    vids = []
    for fp in path.list_files(PROC_VIDEO_DIR):
        vids.append(process_vid(fp))

    with open(path.here(__file__, "../assets/data/videos.json"), "w") as f:
        f.write(dada_serde.obj_to_json(vids))


if __name__ == "__main__":
    main()
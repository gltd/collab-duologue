#/bin/sh 

set -x 

echo 'syncing duologue assets'

s3cmd sync assets/img/ s3://gltd-3-dekalb-works-duologue/assets/img/ -P --delete-removed  
s3cmd sync assets/videos/ s3://gltd-3-dekalb-works-duologue/assets/videos/ -P --delete-removed  
s3cmd sync assets/sounds/ s3://gltd-3-dekalb-works-duologue/assets/sounds/ -P --delete-removed  

echo '-------------------------'
echo 'done!'
